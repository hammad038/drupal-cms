<?php

declare(strict_types=1);

namespace Drupal\developer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\developer\Service\DeveloperService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Developer routes.
 */
final class DeveloperController extends ControllerBase {

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The developer service.
   *
   * @var \Drupal\developer\Service\DeveloperService
   */
  protected DeveloperService $developerService;

  /**
   * The controller constructor.
   */
  public function __construct(
    LoggerChannelFactoryInterface $loggerFactory,
    Connection $database,
    EntityTypeManagerInterface $entityTypeManager,
    DeveloperService $developerService,
  ) {
    $this->loggerFactory = $loggerFactory;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->developerService = $developerService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('logger.factory'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('developer.service'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    $this->exampleServiceUse();
    $this->exampleDatabaseFetch();
    $this->exampleControllerMethod();

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Developer Test Controller Route!'),
    ];

    return $build;
  }

  /**
   * Example method to test some functionality during development.
   *
   * @return void
   */
  protected function exampleControllerMethod(): void {
    // @todo Write your test code.
    syslog(4, '');
  }

  /**
   * Use of custom developer service method from controller.
   *
   * @return void
   */
  protected function exampleServiceUse(): void {
    $this->developerService->doSomething();
  }

  /**
   * Use of database query to fetch data from database tables.
   *
   * @return array
   *
   * @throws \Drupal\Core\Database\DatabaseException
   */
  protected function exampleDatabaseFetch(): array {
    $users = [];

    try {
      $query = $this->database->select('users_field_data', 'ufd')
        ->fields('ufd', ['uid', 'name', 'mail'])
        ->range(0, 10);

      $users = $query->execute()->fetchAll();
    }
    catch (DatabaseException $e) {
      $this->loggerFactory->get('developer')->error('Database query failed: @message', [
        '@message' => $e->getMessage(),
      ]);

      throw $e;
    }

    return $users;
  }

}
