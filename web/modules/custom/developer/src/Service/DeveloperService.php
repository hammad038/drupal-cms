<?php

declare(strict_types=1);

namespace Drupal\developer\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Developer Service class.
 */
final class DeveloperService {

  /**
   * Constructs a Service object.
   */
  public function __construct(
    private readonly Connection $connection,
    private readonly LoggerChannelInterface $loggerChannelDefault,
    private readonly RouteMatchInterface $routeMatch,
    private readonly RequestStack $requestStack,
    private readonly AccountProxyInterface $currentUser,
  ) {}

  /**
   * Some method of developer service.
   */
  public function doSomething(): void {
    $this->loggerChannelDefault->info('This is a log from DeveloperService.');
  }

}
